﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;

namespace TP3_AFHBS.Controllers
{
    public class VehiculesController : Controller
    {
        private TP3Entities db = new TP3Entities();
        private IMapper mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Vehicule, VehiculeViewModel>();
            })
            .CreateMapper();

        // GET: Vehicules
        public ActionResult Index()
        {
            List<VehiculeViewModel> vehiculeVMs = new List<VehiculeViewModel>();

            foreach (Vehicule vehicule in db.Vehicules)
            {
                vehiculeVMs.Add(mapper.Map<Vehicule, VehiculeViewModel>(vehicule));
            }

            return View(vehiculeVMs);
        }

        [HttpPost]
        public ActionResult Index(bool? ImagePresent, string SearchTerm)
        {
            List<VehiculeViewModel> vehiculeVMs = new List<VehiculeViewModel>();

            List<Vehicule> vehicules = db.Vehicules.Where(v => ImagePresent.Value ? v.Images.Count > 0 : true && (v.Marque + v.Model + v.Description).Contains(SearchTerm)).ToList();

            foreach (Vehicule vehicule in vehicules)
            {
                vehiculeVMs.Add(mapper.Map<Vehicule, VehiculeViewModel>(vehicule));
            }

            return View(vehiculeVMs);
        }

        // GET: Vehicules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicule vehicule = db.Vehicules.Find(id);
            if (vehicule == null)
            {
                return HttpNotFound();
            }

            VehiculeViewModel vehiculeVM = mapper.Map<Vehicule, VehiculeViewModel>(vehicule);

            return View(vehiculeVM);
        }

        // GET: Vehicules/Create
        [AdminOnly]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicules/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [AdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VehiculeID,Type,Marque,Model,PrixVente,SoldePourcentage,IsNeuf,DateMiseEnVente,Description")] Vehicule vehicule)
        {
            if (ModelState.IsValid)
            {
                db.Vehicules.Add(vehicule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vehicule);
        }


        // GET: Vehicules/Edit/5
        [AdminOnly]
        [OutputCache(Duration = 0)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicule vehicule = db.Vehicules.Find(id);
            if (vehicule == null)
            {
                return HttpNotFound();
            }
            return View(vehicule);
        }

        // POST: Vehicules/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [AdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehiculeID,Type,Marque,Model,PrixVente,SoldePourcentage,IsNeuf,DateMiseEnVente,Description")] Vehicule vehicule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vehicule);
        }

        // GET: Vehicules/Delete/5
        [AdminOnly]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicule vehicule = db.Vehicules.Find(id);
            if (vehicule == null)
            {
                return HttpNotFound();
            }
            return View(vehicule);
        }

        // POST: Vehicules/Delete/5
        [AdminOnly]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehicule vehicule = db.Vehicules.Find(id);
            db.Vehicules.Remove(vehicule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
