﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;

namespace TP3_AFHBS.Controllers
{
    public class ImagesController : Controller
    {
        private TP3Entities db = new TP3Entities();

        // POST: Images/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [AdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ImageURL,VehiculeID")] Image image)
        {
            if (ModelState.IsValid)
            {
                db.Images.Add(image);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VehiculeID = new SelectList(db.Vehicules, "VehiculeID", "Type", image.VehiculeID);
            return View(image);
        }

        // POST: Images/Delete/5
        [AdminOnly]
        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            Console.WriteLine(id);

            Image image = db.Images.Find(id);
            db.Images.Remove(image);
            db.SaveChanges();
            string message = "SUCCESS";
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
