﻿using IronPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Controllers
{
    public class CommandesController : Controller
    {
        private TP3Entities db = new TP3Entities();


        // GET: Commandes
        [SessionActiveOnly]
        public ActionResult Index()
        {
            if ((string)Session[SessionVariables.Type] == UserTypes.Admin)
            {
                var commandes = db.Commandes.Include(c => c.User);
                return View(commandes.ToList());
            }
            else
            {
                string userName = (string)Session[SessionVariables.UserName];

                var commandes = db.Commandes.Where(c => c.UserName == userName);
                return View(commandes.ToList());
            }
        }

        // GET: Commandes/Details/5
        [SessionActiveOnly]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Commande commande = db.Commandes.Find(id);

            if ((string)Session[SessionVariables.Type] != UserTypes.Admin && (string)Session[SessionVariables.UserName] != commande.UserName)
            {
                return new RedirectResult("~/Home/NotAuthorised");
            }
            if (commande == null)
            {
                return HttpNotFound();
            }
            return View(commande);
        }
        // GET: Commandes/Edit/5
        [SessionActiveOnly]
        [AdminOnly]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Commande commande = db.Commandes.Find(id);

            if ((string)Session[SessionVariables.Type] != UserTypes.Admin && (string)Session[SessionVariables.UserName] != commande.UserName)
            {
                return new RedirectResult("~/Home/NotAuthorised");
            }
            if (commande == null)
            {
                return HttpNotFound();
            }
            //Créer la liste à afficher dans le DropDownList des statuts
            List<string> listStatus = new List<string>();
            foreach (string st in CommandeStatuts.Display.Keys)
            {
                listStatus.Add(st);
            }

            List<string> modePaiements = new List<string>();

            modePaiements.Add(null);

            foreach (string mp in ModesPaiement.Display.Keys)
            {
                modePaiements.Add(mp);
            }


            ViewBag.ModePaiement = new SelectList(modePaiements);

            ViewBag.Statut = new SelectList(listStatus);

            return View(commande);
        }
        
        // POST: Commandes/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [SessionActiveOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CommandeID,Date,Statut,UserName,ModePaiement")] Commande commande)
        {
            if (ModelState.IsValid)
            {
                Commande com = db.Commandes.Find(commande.CommandeID);
                com.Statut = commande.Statut;
                com.ModePaiement = commande.ModePaiement;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserName = new SelectList(db.Users, "UserName", "MdpHash", commande.UserName);
            return View(commande);
        }

        public ActionResult Finalise(int id)
        {
            Commande commande = db.Commandes.Find(id);

            return View(commande);
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult PrintDOC(string BDCContent)
        {

            HtmlToPdf renderer = new IronPdf.HtmlToPdf();
            byte[] pdf = renderer.RenderHtmlAsPdf(BDCContent).BinaryData;

            return File(pdf, "application/pdf", "document-" + DateTime.Now.Date + ".pdf");


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
