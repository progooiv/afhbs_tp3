﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;

namespace TP3_AFHBS.Controllers
{
    public class CommadeVehiculesController : Controller
    {
        private TP3Entities db = new TP3Entities();

        // GET: CommadeVehicules
        [AdminOnly]
        public ActionResult Index()
        {
            var commadeVehicules = db.CommadeVehicules.Include(c => c.Commande).Include(c => c.Vehicule);
            return View(commadeVehicules.ToList());
        }

        // GET: CommadeVehicules/Details/5
        [SessionActiveOnly]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommadeVehicule commadeVehicule = db.CommadeVehicules.Find(id);

            
            if (commadeVehicule == null)
            {
                return HttpNotFound();
            }

            if ((string)Session[SessionVariables.Type] != UserTypes.Admin && (string)Session[SessionVariables.UserName] != commadeVehicule.Commande.UserName)
            {
                return new RedirectResult("~/Home/NotAuthorised");
            }

            ViewBag.VehiculeTitre = commadeVehicule.Vehicule.Marque + " " + commadeVehicule.Vehicule.Model;

            return View(commadeVehicule);
        }

        // GET: CommadeVehicules/Create
        [SessionActiveOnly]
        public ActionResult Create(int? id)
        {
            string UserName = (string)Session[SessionVariables.UserName];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommadeVehicule cv = new CommadeVehicule();
            if ((string)Session[SessionVariables.Type] == UserTypes.Normal)
            {
                ViewBag.UserName = UserName;
            }
            else if ((string)Session[SessionVariables.Type] == UserTypes.Admin)
            {
                ViewBag.UserName = new SelectList(db.Users.Where(u => u.Type != UserTypes.Admin), "UserName", "UserName");
            }
            else if ((string)Session[SessionVariables.Type] == UserTypes.Societe)
            {
                ViewBag.UserName = UserName;
            }

            cv.Quantite = 1;
            cv.Vehicule = db.Vehicules.Find(id);
            cv.VehiculeID = id.Value;

            ViewBag.Options = new SelectList(db.Options.Where(o => o.VehiculeID == cv.VehiculeID), "OptionID", "Description");

            ViewBag.Prix = cv.Vehicule.SoldePourcentage == null ? cv.Vehicule.PrixVente : cv.Vehicule.PrixVente * ((100 - cv.Vehicule.SoldePourcentage) / 100);

            return View(cv);
        }

        // POST: CommadeVehicules/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [SessionActiveOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int? VehiculeID, string UserName, int? Quantite, int[] Options)
        {
            CommadeVehicule cv = new CommadeVehicule();

            if ((string)Session[SessionVariables.Type] != UserTypes.Admin)
            {
                UserName = (string)Session[SessionVariables.UserName];
            }

            if (Quantite == null || VehiculeID == null || UserName == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            { 
                Commande commande = db.Commandes.Where(c => c.UserName == UserName && c.Statut == CommandeStatuts.EnCours).FirstOrDefault();

                if (commande == null)
                {
                    commande = new Commande()
                    {
                        Date = DateTime.Now,
                        Statut = CommandeStatuts.EnCours,
                        UserName = UserName
                    };
                    db.Commandes.Add(commande);
                    db.SaveChanges();
                }

                cv.VehiculeID = VehiculeID.Value;

                cv.CommandeID = commande.CommandeID;

                cv.Quantite = Quantite;

                db.CommadeVehicules.Add(cv);
                db.SaveChanges();

                if (Options != null)
                {
                    foreach (int optionID in Options)
                    {
                        OptionCommande oc = new OptionCommande();

                        oc.OptionID = optionID;
                        oc.CommandeVehiculeID = cv.CommandeVehiculeID;

                        db.OptionCommandes.Add(oc);

                        db.SaveChanges();
                    }
                }
            }


            return View("Details", db.CommadeVehicules.Find(cv.CommandeVehiculeID));
        }

        // GET: CommadeVehicules/Edit/5
        [AdminOnly]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommadeVehicule commadeVehicule = db.CommadeVehicules.Find(id);
            if (commadeVehicule == null)
            {
                return HttpNotFound();
            }
            ViewBag.CommandeID = new SelectList(db.Commandes, "CommandeID", "Statut", commadeVehicule.CommandeID);
            ViewBag.VehiculeID = new SelectList(db.Vehicules, "VehiculeID", "Type", commadeVehicule.VehiculeID);
            return View(commadeVehicule);
        }

        // POST: CommadeVehicules/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [AdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CommandeVehiculeID,VehiculeID,CommandeID")] CommadeVehicule commadeVehicule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commadeVehicule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CommandeID = new SelectList(db.Commandes, "CommandeID", "Statut", commadeVehicule.CommandeID);
            ViewBag.VehiculeID = new SelectList(db.Vehicules, "VehiculeID", "Type", commadeVehicule.VehiculeID);
            return View(commadeVehicule);
        }

        // POST: CommadeVehicules/Delete/5
        [AdminOnly]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CommadeVehicule commadeVehicule = db.CommadeVehicules.Find(id);
            db.CommadeVehicules.Remove(commadeVehicule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
