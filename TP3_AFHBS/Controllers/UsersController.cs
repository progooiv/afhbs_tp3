﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;
using TP3_AFHBS.Models.Hashing;

namespace TP3_AFHBS.Controllers
{
    public class UsersController : Controller
    {
        private TP3Entities db = new TP3Entities();
        private IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<User, UserViewModel>();
            cfg.CreateMap<User, RegisterViewModel>();
        }).CreateMapper();


        // GET: Users
        [AdminOnly]
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: Users/Details/5
        [SessionActiveOnly]
        public ActionResult Details()
        {
            string sessionUserName = (string)Session[SessionVariables.UserName];
            // TODO envioi clients et admin aux bonnes places
            UserViewModel user = mapper.Map<User, UserViewModel>(db.Users.Where(u => u.UserName == sessionUserName).FirstOrDefault());
            if (user.Type == UserTypes.Admin)
            {
                return RedirectToAction("AdminView");
            }
            else
            {
                return View(user);
            }
        }

        [AdminOnly]
        public ActionResult AdminView()
        {
            string sessionUserName = (string)Session[SessionVariables.UserName];
            // TODO envioi clients et admin aux bonnes places
            UserViewModel user = mapper.Map<User, UserViewModel>(db.Users.Where(u => u.UserName == sessionUserName).FirstOrDefault());
            return View(user);
        }

        // GET: Users/Create
        [AdminOnly]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [AdminOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserName,MdpHash,Type,Nom,Email")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        [SessionActiveOnly]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if ((string)Session[SessionVariables.Type] != UserTypes.Admin && (string)Session[SessionVariables.UserName] != id) {
                return new RedirectResult("~/Home/NotAuthorised");
            }
            RegisterViewModel user = mapper.Map<User, RegisterViewModel>(db.Users.Find(id));
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [SessionActiveOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserName, Nom, Email, Password, PasswordConfirm")] RegisterViewModel userRegister)
        {
            if ((string)Session[SessionVariables.Type] != UserTypes.Admin && (string)Session[SessionVariables.UserName] != userRegister.UserName)
            {
                return new RedirectResult("~/Home/NotAuthorised");
            }
            if (ModelState.IsValid)
            {
                /* User user = new User()
                 {
                     UserName = userRegister.UserName,
                     MdpHash = PasswordHasher.Generate(userRegister.Password),
                     Nom = userRegister.Nom,
                     Email = userRegister.Email
                 };*/

                using (TP3Entities db = new TP3Entities())
                {
                    try
                    {
                        User user = db.Users.Where(u => u.UserName == userRegister.UserName).FirstOrDefault();
                        user.MdpHash = PasswordHasher.Generate(userRegister.Password);
                        Console.WriteLine(user.MdpHash.Length);
                        user.Nom = userRegister.Nom;
                        user.Email = userRegister.Email;
                        db.SaveChanges();

                        return RedirectToAction("Details", "Users");
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                        return RedirectToAction("Edit", "Users", new { Erreur = "Impossible d'appliquer les modifications." });
                    }
                }
            }
            else
            {
                return View();
            }
        }

        // GET: Users/Delete/5
        [AdminOnly]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [AdminOnly]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        */
    }
}
