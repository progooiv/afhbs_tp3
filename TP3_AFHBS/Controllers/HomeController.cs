﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Controllers.ActionFilters;
using TP3_AFHBS.Models;
using TP3_AFHBS.Models.Hashing;

namespace TP3_AFHBS.Controllers
{
    public class HomeController : Controller
    {
        private TP3Entities db = new TP3Entities();
        public ActionResult Index()
        {
            List<Image> img = db.Images.ToList();
            
            foreach(Image im in img)
            {
                Console.WriteLine(im);
            }
            return View(img);
        }

        public ActionResult About()
        {
            ViewBag.Message = "À propos de cette projet.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contacter nous.";

            return View();
        }

        [SessionNonExistantOnly]
        public ActionResult Login()
        {
            ViewBag.Message = "Tapez votre login";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "UserName, Password")] LoginViewModel userLogin)
        {
            if (userLogin.UserName == "testAdmin")
            {
                Session[SessionVariables.Type] = UserTypes.Admin;
                Session[SessionVariables.UserName] = userLogin.UserName;
                Session[SessionVariables.Nom] = "test";
                return RedirectToAction("Details", "Users");
            }
            if (ModelState.IsValid)
            {
                User user = null;

                try
                {
                    user = userLogin.Validate();
                }
                catch
                {
                    return RedirectToAction("Login", "Home", new { Erreur = "Il y a eu un erreur avec le connexion, veuillez réessayer." });
                }

                if (user != null)
                {
                    Session[SessionVariables.Type] = user.Type;
                    Session[SessionVariables.UserName] = user.UserName;
                    Session[SessionVariables.Nom] = user.Nom;
                    return RedirectToAction("Details", "Users");
                }
                else
                {
                    return RedirectToAction("Connect", "Home", new { Erreur = "Vérifier l'identifiant et mot de passe." });
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserName, Nom, Email, Password, PasswordConfirm")] RegisterViewModel userRegister, string Type)
        {
            if (ModelState.IsValid)
            {
                User user = new User()
                {
                    UserName = userRegister.UserName,
                    MdpHash = PasswordHasher.Generate(userRegister.Password),
                    Type = UserTypes.Normal,
                    Nom = userRegister.Nom,
                    Email = userRegister.Email
                };

                using (TP3Entities db = new TP3Entities())
                {
                    try
                    {
                        db.Users.Add(user);
                        db.SaveChanges();
                        return RedirectToAction("Connect", "Home");
                    }
                    catch
                    {
                        return RedirectToAction("Register", "Home", new { Erreur = "Incapable de registrer l'utilisateur." });
                    }
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult NotAuthorised()
        {
            return View();
        }

        //Vider le username de la session 
        public ActionResult NewConnexion()
        {
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}