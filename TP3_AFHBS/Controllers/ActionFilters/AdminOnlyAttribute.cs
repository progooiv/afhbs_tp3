﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3_AFHBS.Models;

namespace TP3_AFHBS.Controllers.ActionFilters
{
    public class AdminOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctxt = HttpContext.Current;
            if ((string)HttpContext.Current.Session[SessionVariables.Type] != "Admin")
            {
                filterContext.Result = new RedirectResult("~/Home/NotAuthorised");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}