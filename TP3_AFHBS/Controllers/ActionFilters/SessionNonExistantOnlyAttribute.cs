﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TP3_AFHBS.Controllers.ActionFilters
{
    public class SessionNonExistantOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctxt = HttpContext.Current;
            if (HttpContext.Current.Session["UserName"] != null)
            {
                filterContext.Result = new RedirectResult("~/Users/Details");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}