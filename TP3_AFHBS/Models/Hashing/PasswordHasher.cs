﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace TP3_AFHBS.Models.Hashing
{
    public static class PasswordHasher
    {
        public static string Generate(string password, int iterations = 1000)
        {
            //  Generate salt.
            var salt = new byte[24];
            new RNGCryptoServiceProvider().GetBytes(salt);

            //  Generate hash.
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = pbkdf2.GetBytes(24);

            //  Return concatenation : salt|iterations|hash.
            return Convert.ToBase64String(salt) + "|" + iterations + "|" + Convert.ToBase64String(hash);
        }

        private static string Generate(string password, byte[] salt, int iterations)
        {
            //  Generate hash.
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = pbkdf2.GetBytes(24);

            return Convert.ToBase64String(hash);
        }

        public static bool IsValid(string testPassword, string delimitedHash)
        {
            string[] hashParts = delimitedHash.Split('|');
            byte[] salt = Convert.FromBase64String(hashParts[0]);
            int iterations = Int32.Parse(hashParts[1]);
            string hash = hashParts[2];

            return Generate(testPassword, salt, iterations) == hash;
        }
    }
}