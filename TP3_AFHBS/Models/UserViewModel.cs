﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Models
{
    public class UserViewModel
    {
        [Key]
        [Display(Name = NameLibrary.USER_DISP_USERNAME)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string UserName { get; set; }
        //***************************************** Retablie
        [Display(Name = "Mot de passe")]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string MdpHash { get; set; }
        public string Type { get; set; }
        [Display(Name = NameLibrary.USER_DISP_NOM)]
        public string Nom { get; set; }
        [Display(Name = NameLibrary.USER_DISP_EMAIL)]
        public string Email { get; set; }
    }
}