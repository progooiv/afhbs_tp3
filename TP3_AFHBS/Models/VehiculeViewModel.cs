﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Models
{
    public class VehiculeViewModel
    {
        [Key]
        [Display(Name = NameLibrary.VEH_DISP_ID)]
        public int VehiculeID { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_TYPE)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string Type { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_MOD)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string Marque { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_MARQUE)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string Model { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_PRIX)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public decimal PrixVente { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_SOLDE)]
        public Nullable<decimal> SoldePourcentage { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_NEUF)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public bool IsNeuf { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_DATE)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public System.DateTime DateMiseEnVente { get; set; }

        [Display(Name = NameLibrary.VEH_DISP_DESC)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Image> Images { get; set; }

        public decimal PrixAvecSolde
        {
            get => PrixVente * (SoldePourcentage == null ? 1 : (100 - SoldePourcentage.Value) / 100);
        }
    }
}