﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models
{
    public static class UserTypes
    {
        public static String Admin { get => "Admin"; }
        public static String Normal { get => "Normal"; }
        public static String Societe { get => "Societe"; }
    }
}