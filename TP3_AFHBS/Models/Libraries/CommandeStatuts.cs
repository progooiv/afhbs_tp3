﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models
{
    public class CommandeStatuts
    {
        public static string EnCours { get => "EnCours"; }
        public static string Aboutie { get => "Aboutie"; }
        public static string Annulee { get => "Annulee"; }

        public static Dictionary<string, string> Display
        {
            get => new Dictionary<string, string>()
            {
                [EnCours] = "En cours",
                [Aboutie] = "Terminée",
                [Annulee] = "Annulée"
            };
        }
    }
}