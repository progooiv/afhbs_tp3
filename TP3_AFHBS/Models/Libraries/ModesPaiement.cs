﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models.Libraries
{
    public class ModesPaiement
    {
        public static string Comptant { get => "Comptant"; }
        public static string Credit { get => "Credit"; }

        public static Dictionary<string, string> Display
        {
            get => new Dictionary<string, string>()
            {
                [Comptant] = "Comptant",
                [Credit] = "Crédit"
            };
        }

        public string Text { get; set; }
        public string Value { get; set; }
    }
}