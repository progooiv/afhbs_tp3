﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models.Libraries
{
    public static class ValidationLibrary
    {
        public const string ERR_REQUIS = "Ce champ est requis.";
        public const string ERR_PASSWORD = "Le mot de passe doit contenir 8 charactères dont au moins 1 lettre et 1 chiffre";
        public const string ERR_PASSWORD_CONF = "Les mot de passe ne sont pas parreils";
        public const string ERR_EMAIL = "Adresse courriel non-valide";
        public const string ERR_USERNAME = "Le nom de utilisateur doit contenir au moins 3 charactères";
        public const string ERR_NOM = "Le nom doit contenir au moins 3 charactères";

        public const string REGEX_EMAIL = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        public const string REGEX_PASSWORD = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$";
        public const string REGEX_NOM = "^\\w{3,50}$";
        public const string REGEX_USERNAME = "^\\w{3,50}$";
    }
}