﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models.Libraries
{
    public static class NameLibrary
    {
        public const string USER_DISP_NOM = "Nom complet";
        public const string USER_DISP_USERNAME = "Nom d'utilisateur";
        public const string USER_DISP_PASSWORD = "Mot de passe";
        public const string USER_DISP_PASSWORD_CONF = "Confirmation du mot de passe";
        public const string USER_DISP_EMAIL = "Courriel";

        public const string VEH_DISP_ID = "ID";
        public const string VEH_DISP_TYPE = "Type";
        public const string VEH_DISP_MARQUE = "Marque";
        public const string VEH_DISP_MOD = "Modèle";
        public const string VEH_DISP_PRIX = "Prix";
        public const string VEH_DISP_SOLDE = "Solde";
        public const string VEH_DISP_NEUF = "Neuf";
        public const string VEH_DISP_DATE = "Date";
        public const string VEH_DISP_DESC = "Description";

        public const string COM_DISP_ID = "N ͦ. Commande";
    }
}