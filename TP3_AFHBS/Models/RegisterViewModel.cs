﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Models
{
    public class RegisterViewModel
    {
        public RegisterViewModel()
        {

        }

        [Key]
        [Display(Name = NameLibrary.USER_DISP_USERNAME)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        [RegularExpression(
            ValidationLibrary.REGEX_USERNAME, 
            ErrorMessage = ValidationLibrary.ERR_USERNAME)]
        public string UserName { get; set; }

        public string Type { get; set; }

        [Display(Name = NameLibrary.USER_DISP_NOM)]
        public string Nom { get; set; }

        [Display(Name = NameLibrary.USER_DISP_EMAIL)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        [RegularExpression(
            ValidationLibrary.REGEX_EMAIL, 
            ErrorMessage = ValidationLibrary.ERR_EMAIL)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = NameLibrary.USER_DISP_PASSWORD)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        [RegularExpression(
            ValidationLibrary.REGEX_PASSWORD,
            ErrorMessage = ValidationLibrary.ERR_PASSWORD)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = NameLibrary.USER_DISP_PASSWORD_CONF)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        [Compare(
            "Password", 
            ErrorMessage = ValidationLibrary.ERR_PASSWORD_CONF)]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

        
    }
}