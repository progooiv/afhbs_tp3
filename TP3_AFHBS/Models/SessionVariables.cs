﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models
{
    public class SessionVariables
    {
        public static string UserName { get => "UserName"; }
        public static string Type { get => "Type"; }
        public static string Nom { get => "Nom"; }
    }
}