﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TP3_AFHBS.Models.Hashing;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Models
{
    public class LoginViewModel
    {
        [Key]
        [Display(Name =NameLibrary.USER_DISP_USERNAME)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        public string UserName { get; set; }

        [Display(Name = NameLibrary.USER_DISP_PASSWORD)]
        [RegularExpression(
            ValidationLibrary.REGEX_PASSWORD,
            ErrorMessage = ValidationLibrary.ERR_PASSWORD)]
        [Required(ErrorMessage = ValidationLibrary.ERR_REQUIS)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public User Validate()
        {
            using(TP3Entities db = new TP3Entities())
            {
                User user = db.Users.Where(u => u.UserName == UserName).FirstOrDefault();

                if (PasswordHasher.IsValid(Password, user.MdpHash))
                {
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}