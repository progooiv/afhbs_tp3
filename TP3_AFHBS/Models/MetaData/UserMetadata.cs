﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models
{
    public class UserMetadata
    {
        
        public string UserName { get; set; }
       
        public string MdpHash { get; set; }
        public string Type { get; set; }
        public string Nom { get; set; }
        public string Email { get; set; }
    }
}