﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models.MetaData
{
    public class VehiculeMetadata
    {

        public int VehiculeID { get; set; }
        public string Type { get; set; }
        public string Marque { get; set; }
        public string Model { get; set; }
        [Display(Name = "Prix de vente")]
        public decimal PrixVente { get; set; }
        [Display(Name = "Pourcentage du solde")]
        public Nullable<decimal> SoldePourcentage { get; set; }
        public bool IsNeuf { get; set; }
        [Display(Name = "Date de Mise en vente")]
        public System.DateTime DateMiseEnVente { get; set; }
        public string Description { get; set; }
    }
}