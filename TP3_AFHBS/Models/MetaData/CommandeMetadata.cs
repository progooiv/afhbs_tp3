﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TP3_AFHBS.Models.Libraries;

namespace TP3_AFHBS.Models.MetaData
{
    public class CommandeMetadata
    {
        [Display(Name = NameLibrary.COM_DISP_ID)]
        public int CommandeID { get; set; }
        [Display(Name =NameLibrary.VEH_DISP_DATE)]
        [DisplayFormat(DataFormatString = "{0:MMMM/dd/yyyy}")]
        public System.DateTime Date { get; set; }
        public string Statut { get; set; }
        [Display(Name =NameLibrary.USER_DISP_USERNAME)]
        public string UserName { get; set; }
    }
}