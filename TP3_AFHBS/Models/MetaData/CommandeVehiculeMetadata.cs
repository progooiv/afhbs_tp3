﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TP3_AFHBS.Models.MetaData
{
    public class CommandeVehiculeMetadata
    {
        [Display(Name = "Commande Vehicule ID")]
        public int CommandeVehiculeID { get; set; }
        [Display(Name = "Vehicule ID")]
        public int VehiculeID { get; set; }
        [Display(Name = "Commande ID")]
        public int CommandeID { get; set; }
    }
}